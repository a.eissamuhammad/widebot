import { ENDPOINT } from './endpoint/endpoint';
import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, delay } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private ENDPOINT = ENDPOINT;
  private http = inject(HttpClient)

  // we add pipe delete for handle loading

  getUsers(): Observable<any[]> {
    return this.http.get<any[]>(this.ENDPOINT.MAIN_HOST + this.ENDPOINT.USERS.USER_LIST).pipe(delay(2000));
  }

  getUserById(id: number): Observable<any> {
    return this.http.get<any>(this.ENDPOINT.MAIN_HOST + this.ENDPOINT.USERS.USER_BY_ID + id).pipe(delay(2000));
  }

  addUser(user: any): Observable<any> {
    return this.http.post<any>(this.ENDPOINT.MAIN_HOST + this.ENDPOINT.USERS.USER_BY_ID, user).pipe(delay(2000));
  }

  updateUser(user: any): Observable<any> {
    return this.http.put<any>(this.ENDPOINT.MAIN_HOST + this.ENDPOINT.USERS.USER_BY_ID + user.id, user).pipe(delay(2000));
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete<any>(this.ENDPOINT.MAIN_HOST + this.ENDPOINT.USERS.USER_DELETE + id).pipe(delay(2000));
  }

}
