import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private users = {
    admin: { username: 'admin', password: 'admin', role: 'admin' },
    user: { username: 'user', password: 'user', role: 'user' }
  };
  private loggedInUser: any;

  constructor(private router: Router) {}

  login(username: string, password: string): boolean {
    const user = Object.values(this.users).find(u => u.username === username && u.password === password);
    if (user) {
      this.loggedInUser = user;
      this.router.navigate(['/Dashboard']);
      return true;
    }
    return false;
  }

  logout(): void {
    this.loggedInUser = null;
    this.router.navigate(['/login']);
  }

  getRole(): string {
    return this.loggedInUser ? this.loggedInUser.role : null;
  }

  isLoggedIn(): boolean {
    return !!this.loggedInUser;
  }

}
