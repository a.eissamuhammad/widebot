
export const ENDPOINT = {
    MAIN_HOST: 'https://jsonplaceholder.typicode.com/',
    GENERAL: {
    },
    USERS: {
        USER_LIST: 'users?_start=0&_limit=10',
        USER_DELETE: 'users/',
        USER_BY_ID: 'users/'
    },
  };
