import { Routes } from '@angular/router';
import { authGuard } from '@Core/guards/auth.guard';
import { LayoutComponent } from '@Core/layout/layout.component';
import { provideTranslocoScope } from '@jsverse/transloco';

export const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'Home' },
    {
        path: 'Home',
        component: LayoutComponent,
        loadChildren: () => import('./modules/home/home.routes')
    },
    {
        path: 'Dashboard',
        canActivate: [authGuard],
        component: LayoutComponent,
        loadChildren: () => import('./modules/dashboard/dashboard.routes'),
        providers: [
            provideTranslocoScope('dashboard'),
        ],
    },
    {
        path: 'Login',
        component: LayoutComponent,
        loadChildren: () => import('./modules/auth/login/login.routes')
    }

];