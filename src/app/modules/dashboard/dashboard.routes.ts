import { DashboardComponent } from './dashboard.component';
import { Routes } from '@angular/router';

export default [
    {
        path     : '',
        component: DashboardComponent,
        data: { title: 'DashBoard' }
    },
] as Routes;
