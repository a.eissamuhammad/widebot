import { AuthService } from '@Shared/services/auth.service';
import { Component, inject } from '@angular/core';
import { UsersComponent } from "./users/users.component";
import { ProfileComponent } from "./profile/profile.component";

@Component({
    selector: 'app-dashboard',
    standalone: true,
    templateUrl: './dashboard.component.html',
    styleUrl: './dashboard.component.scss',
    imports: [UsersComponent, ProfileComponent]
})
export class DashboardComponent {

  authService = inject(AuthService);

}
