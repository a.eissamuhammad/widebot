import { UserService } from '@Shared/services/user.service';
import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { TranslocoModule } from '@jsverse/transloco';
import { EMPTY, catchError } from 'rxjs';

@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [CommonModule,TranslocoModule],
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.scss'
})
export class ProfileComponent {
  userService = inject(UserService);
  loadingUserError = false;
  // we call static user info for handle VIEW
  user$ = this.userService.getUserById(1).pipe(
    catchError(() => { this.loadingUserError = true; return EMPTY })
  )
}
