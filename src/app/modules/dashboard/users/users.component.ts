import { UserService } from '@Shared/services/user.service';
import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { TranslocoModule } from '@jsverse/transloco';
import { EMPTY, catchError } from 'rxjs';

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [CommonModule,TranslocoModule],
  templateUrl: './users.component.html',
  styleUrl: './users.component.scss'
})
export class UsersComponent {
  userService = inject(UserService);
  loadingUserError = false;
  users$ = this.userService.getUsers().pipe(
    catchError(() => { this.loadingUserError = true; return EMPTY })
  )


  ngOnInit(): void {

  }


  deleteUser(userId: number) {
    if (confirm('Are you sure you want to delete this user?')) {
      this.userService.deleteUser(userId).subscribe(() => {
        alert("user delete success")
      });
    }
  }
}
