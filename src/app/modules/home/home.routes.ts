import { HomeComponent } from './home.component';
import { Routes } from '@angular/router';

export default [
    {
        path     : '',
        component: HomeComponent,
        data: { title: 'Home' }
    },
] as Routes;
