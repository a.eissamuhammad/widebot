import { Routes } from '@angular/router';
import { LoginComponent } from './login.component';

export default [
    {
        path     : '',
        component: LoginComponent,
        data: { title: 'Login' }
    },
] as Routes;
