import { AuthService } from '@Shared/services/auth.service';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule,CommonModule,TranslocoModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {

  LoginForm!: UntypedFormGroup;
  errorMessage!: string;

  constructor(
    private authService: AuthService,
    private formBuilder: UntypedFormBuilder,
    private router: Router,
  ) {
  }

  ngOnInit(): void {

    this.LoginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
    });
  }


  Login(){
    if (!this.authService.login(this.LoginForm.value.username, this.LoginForm.value.password)) {
      this.errorMessage = 'Invalid credentials';
    }
  }


}
