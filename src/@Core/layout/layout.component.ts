import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooterComponent } from "./component/footer/footer.component";
import { HeaderComponent } from "./component/header/header.component";

@Component({
    selector: 'app-layout',
    standalone: true,
    templateUrl: './layout.component.html',
    styleUrl: './layout.component.scss',
    imports: [RouterModule, FooterComponent, HeaderComponent]
})
export class LayoutComponent {

}
