import { MenuItem } from "@Data/models/layout/menu";

export class Menu {
  public static pages: MenuItem[] = [
    {
      group: '',
      separator: false,
      items: [
        {
          label: 'Home',
          route: '/Home',
        },
        {
          label: 'Admin',
          route: '/Dashboard',
        }
      ],
    }
  ];
}