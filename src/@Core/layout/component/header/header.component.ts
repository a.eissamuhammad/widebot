import { Component, inject } from '@angular/core';
import { Menu } from '../menu';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthService } from '@Shared/services/auth.service';
import { LanguageComponent } from "../language/language.component";
import { TranslocoModule } from '@jsverse/transloco';

@Component({
    selector: 'app-header',
    standalone: true,
    templateUrl: './header.component.html',
    styleUrl: './header.component.scss',
    imports: [CommonModule, RouterModule, LanguageComponent,TranslocoModule]
})
export class HeaderComponent {
  menu = Menu.pages;
  authService = inject(AuthService);


  logOut(){
    this.authService.logout();
  }

}
