import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { TranslocoModule, TranslocoService } from '@jsverse/transloco';

@Component({
  selector: 'language',
  standalone: true,
  imports: [CommonModule,TranslocoModule],
  templateUrl: './language.component.html',
  styleUrl: './language.component.scss'
})
export class LanguageComponent {
  translocoService = inject(TranslocoService)
  currentLang$ = this.translocoService.langChanges$;

  switchLanguage() {
    const currentLang = this.translocoService.getActiveLang();
    const newLang = currentLang === 'en' ? 'ar' : 'en';
    this.translocoService.setActiveLang(newLang);
    // this.currentLang = newLang
  }


}
