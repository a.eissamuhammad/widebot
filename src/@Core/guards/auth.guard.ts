import { AuthService } from '@Shared/services/auth.service';
import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

export const authGuard: CanActivateFn = (route, state) => {
  //  constructor(private authService: AuthService, private router: Router) {}
  const authService = inject(AuthService);
  const router = inject(Router);

    if (authService.isLoggedIn()) {
      return true;
    }
    router.navigate(['/Login']);
    return false;

};
